#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Program to convert a binary file into HEX bytes and display in text window.
#
# Requires PySide.
#
# By Colin D Bennett, 14 May 2013.

import re
import argparse
import os
import sys

def main():
    parser = argparse.ArgumentParser(
            description="Convert a binary file to HEX.")

    parser.add_argument("input", nargs="+", help="specify input file")

    args = parser.parse_args()

    if len(args.input) > 1:
        sys.stderr.write("Can't handle multiple input files yet")
        sys.exit(1)  # Exit with error code

    for infile in args.input:
        hex_str = hexify_file(infile)
        display_hex(hex_str, infile)

# Convert infile from hex to outfile in raw binary.
def hexify_file(infile):
    hexstr = ""
    bytes_on_line = 0
    with open(infile, "rb") as fin:
        b = fin.read(1)
        while b:
            if hexstr != "":
                if bytes_on_line == 16:
                    hexstr += "\n"      # Break line to keep fixed length
                    bytes_on_line = 0
                else:
                    if bytes_on_line == 8:
                        hexstr += " "   # Extra space between 8 byte groups
                    hexstr += " "       # Delimiter between bytes

            hexstr += "%02X" % ord(b)   # Format byte value as hex
            bytes_on_line += 1
            b = fin.read(1)
    return hexstr


def display_hex(hex_str, filename):
    import PySide
    from PySide.QtGui import QPushButton
    from PySide.QtGui import QPlainTextEdit
    from PySide.QtGui import QVBoxLayout
    from PySide.QtGui import QDialog
    from PySide.QtGui import QApplication

    app = QApplication(sys.argv)

    dialog = QDialog()

    ok_button = QPushButton("OK")
    ok_button.setDefault(True)
    ok_button.clicked.connect(dialog.close)

    text_edit = QPlainTextEdit()
    text_edit.setPlainText(hex_str)
    text_edit.setReadOnly(True)
    # Set the text area to a fixed-width font for hex value alignment.
    text_edit.setStyleSheet(
            "QWidget { font-family: \"Lucida Console\", Monaco, monospace }")

    layout = QVBoxLayout()
    layout.addWidget(text_edit)
    layout.addWidget(ok_button)

    dialog.setWindowTitle(os.path.basename(filename) + " (HEX)")
    dialog.setLayout(layout)
    dialog.resize(512, 512 / 1.618)
    dialog.exec_()


### Run the main program

if __name__ == "__main__":
    main()


# vim: set ft=python et ts=4 sw=4:
