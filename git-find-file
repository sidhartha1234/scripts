#!/bin/sh
#
# Search for a file by name in all git commits in a repository.
#
# Colin D Bennett, 4 Sep 2013
#
# Based on Stack Overflow answer by albfan:
# http://stackoverflow.com/a/16868704/994153
#
# Sometimes you need to search for something that can appear/disappear
# over time, so why not search against all commits? Besides that,
# sometimes you need a verbose response, other times only commit
# mattters. Specify the -v flag to get the full filenames that match.
#

usage() {
    echo "Usage: $0 [-v] branch|--all regex" >&2
    exit 1
}


VERBOSE_FLAG=0
if [ $1 = "-v" ]; then
    VERBOSE_FLAG=1
    shift   # Eat the -v flag
fi

[ $# -ge 1 ] || usage
BRANCH_SPEC="$1"
shift   # Eat the -v flag

[ $# -eq 1 ] || usage
FILENAME_REGEX="$1"
shift   # Eat the filename regex

for branch in $(git rev-list "${BRANCH_SPEC}")
do
    if [ "$VERBOSE_FLAG" = 1 ]
    then
        # Verbose output:
        #    sha1: path/to/<regex>/searched
        #    sha1: path/to/another/<regex>/in/same/sha
        #    sha2: path/to/other/<regex>/in/other/sha
        git ls-tree -r --name-only "$branch" \
            | grep -E "$FILENAME_REGEX" | sed 's/^/'$branch': /'
    else
        # Brief output:
        #    sha1
        #    sha2
        if $(git ls-tree -r --name-only "$branch" \
             | grep -E --quiet "$FILENAME_REGEX") 
        then
            echo "$branch"
        fi
    fi
done

# vim: set et ts=4 sw=4:
