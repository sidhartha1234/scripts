#!/usr/bin/env python
# Convert DRM_MODE(...) array data into CSV file.
# For Established Timings.
# CDB, 14 Jun 2013.

import re
import sys

pat = (
       r"{\s*DRM_MODE\(" +            #      # Macro
       r"\"([^\"]+)\",\s*" +      # nm   # (1) resolution name e.g. 1920x1080
       r"\w+,\s*" +               # t    # ignore 'DRM_MODE_TYPE_DRIVER'
       r"(\d+)\s*,\s*" +          # c    # (2) pixel clock e.g. 74250=74.25MHz
       r"(\d+)\s*,\s*" +          # hd   # (3) H active
       r"(\d+)\s*,\s*" +          # hss  # (4) H sync start
       r"(\d+)\s*,\s*" +          # hse  # (5) H sync end
       r"(\d+)\s*,\s*" +          # ht   # (6) H total
       r"(\d+)\s*,\s*" +          # hsk  # (7) H skew
       r"(\d+)\s*,\s*" +          # vd     (8) V active
       r"(\d+)\s*,\s*" +          # vss    (9) V sync start
       r"(\d+)\s*,\s*" +          # vse   (10) V sync end
       r"(\d+)\s*,\s*" +          # vt    (11) V total
       r"(\d+)\s*,\s*" +          # vs    (12) V scan (always 0)
       r"([^)]+)\)\s*},?\s*" +    # f     (13) flags
       r"/\*\s*(.*?)\s*\*/"       # -     (14) comment (e.g., 800x600@60Hz)
)

def convert_to_csv():
    import csv
    fieldnames = (
        "Name",
        "PixelClock",
        "VActive",
        "VTotal",
        "VBlanking",
        "VFrontPorch",
        "VBackPorch",
        "VSyncWidth",
        "HActive",
        "HTotal",
        "HBlanking",
        "HFrontPorch",
        "HBackPorch",
        "HSyncWidth",
        "HSyncPositive",
        "VSyncPositive",
        "Interlaced",
    )

    with open("modes.csv", "wb") as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(fieldnames)
        parse_and_process(writer)

def parse_and_process(csvwriter):
    # Read the DRM_MODE(...) data.
    content = sys.stdin.read()

    for match in re.finditer(pat, content, re.M):
        nm = match.group(1)
        pixel_clock = int(match.group(2))
        h_active = int(match.group(3))
        h_sync_start = int(match.group(4))
        h_sync_end = int(match.group(5))
        h_total = int(match.group(6))
        h_skew = int(match.group(7))
        v_active = int(match.group(8))
        v_sync_start = int(match.group(9))
        v_sync_end = int(match.group(10))
        v_total = int(match.group(11))
        v_scan = int(match.group(12))  # always 0????
        flags = match.group(13)
        name = match.group(14)

        h_fp = h_sync_start - h_active
        h_synclen = h_sync_end - h_sync_start
        h_bp = h_total - h_sync_end
        h_blanking = h_total - h_active

        v_fp = v_sync_start - v_active
        v_synclen = v_sync_end - v_sync_start
        v_bp = v_total - v_sync_end
        v_blanking = v_total - v_active

        if "NHSYNC" in flags:
            hsync_pol = "0"
        else:
            hsync_pol = "1"

        if "NVSYNC" in flags:
            vsync_pol = "0"
        else:
            vsync_pol = "1"

        # DBLCLK set only for exact doubles? not set for all with pixel replication
        if "DBLCLK" in flags:
            dblclk = "1"
        else:
            dblclk = "0"

        if "INTERLACE" in flags:
            interlaced = "1"
        else:
            interlaced = "0"

        csvwriter.writerow([
            name,
            pixel_clock,
            v_active,
            v_total,
            v_blanking,
            v_fp,
            v_bp,
            v_synclen,
            h_active,
            h_total,
            h_blanking,
            h_fp,
            h_bp,
            h_synclen,
            hsync_pol,
            vsync_pol,
            interlaced,
        ])


if __name__ == "__main__":
    convert_to_csv()


# vim: set et ft=python ts=4 sw=4:
