#!/usr/bin/env python
#
# Automatically convert Google Testing Framework tests to Boost Test Library
# tests.
#
# Colin D Bennett, 2013-12-20.

import argparse
import os
import re
from sys import stdout

def main():
    parser = argparse.ArgumentParser(
            description="Convert Google Test code to Boost Test code.")

    parser.add_argument("--input", required=True, help="input file")
    parser.add_argument("--output", required=True, help="input file")
    parser.add_argument("--quiet", action="store_true", help="quiet")
    parser.add_argument("--verbose", "-v", action="store_true", help="verbose output")

    args = parser.parse_args()

    convert_path(args.input, args.output)


def convert_path(inpath, outpath):
    with open(inpath, "rb") as infile:
        with open(outpath, "wb") as outfile:
            convert_file(infile, outfile)

def convert_file(infile, outfile):
    content = infile.read()

    # Be careful to manually review changes,
    # possibly precedence with "!" use for ASSERT_FALSE etc.
    # may change meaning.  Some uses extra () but not all below.

    content = re.sub(
        r"// Google Testing Framework",
        r"// Boost Test Library headers",
        content)

    content = re.sub(
        r"#include <gtest/gtest.h>",
        r"#include <boost/test/unit_test.hpp>",
        content)

    content = re.sub(
        r"\bTEST\(([^,]+), *([^)]+)\)",
        r"BOOST_AUTO_TEST_CASE(\1__\2)",
        content)

    # With message
    content = re.sub(
        r"\bEXPECT_FALSE\((.*?)\)\s*<<\s*([^;]+);",
        r"BOOST_CHECK_MESSAGE(!\1, \2);",
        content)

    content = re.sub(
        r"\bEXPECT_TRUE\((.*?)\)\s*<<\s*([^;]+);",
        r"BOOST_CHECK_MESSAGE(\1, \2);",
        content)

    content = re.sub(
        r"\bASSERT_FALSE\((.*?)\)\s*<<\s*([^;]+);",
        r"BOOST_REQUIRE_MESSAGE(!\1, \2);",
        content)

    content = re.sub(
        r"\bASSERT_TRUE\((.*?)\)\s*<<\s*([^;]+);",
        r"BOOST_REQUIRE_MESSAGE(\1, \2);",
        content)

    # Boost check equal has no _message variant,
    # just put as a comment for now.
    # It might be better to do BOOST_CHECK_MESSAGE() and
    # include expected + actual as well instead, then we
    # could log the message.
    content = re.sub(
        r"\bEXPECT_EQ\s*\(([^)]+)\)\s*<<\s*([^;]+);",
        r"BOOST_CHECK_EQUAL(\1);  // \2",
        content)

    content = re.sub(
        r"\bASSERT_EQ\s*\((.*?)\)\s*<<\s*([^;]+);",
        r"BOOST_REQUIRE_EQUAL(\1);  // \2",
        content)

    content = re.sub(
        r"\bASSERT_EQ\s*\(([^)]+)\)\s*<<\s*([^;]+);",
        r"BOOST_REQUIRE_EQUAL(\1);  // \2",
        content)

    content = re.sub(
        r"\bEXPECT_STREQ\s*\(([^)]+)\)\s*<<\s*([^;]+);",
        r"BOOST_CHECK_EQUAL(\1);  // \2",
        content)

    content = re.sub(
        r"\bASSERT_STREQ\s*\(([^)]+)\)\s*<<\s*([^;]+);",
        r"BOOST_REQUIRE_EQUAL(\1);  // \2",
        content)


    # Without message
    content = re.sub(
        r"\bEXPECT_FALSE\(",
        r"BOOST_CHECK(!",
        content)

    content = re.sub(
        r"\bEXPECT_TRUE\(",
        r"BOOST_CHECK(",
        content)

    content = re.sub(
        r"\bASSERT_FALSE\(",
        r"BOOST_REQUIRE(!",
        content)

    content = re.sub(
        r"\bASSERT_TRUE\(",
        r"BOOST_REQUIRE(",
        content)


    content = re.sub(
        r"\bEXPECT_EQ\b",
        r"BOOST_CHECK_EQUAL",
        content)

    content = re.sub(
        r"\bASSERT_EQ\b",
        r"BOOST_REQUIRE_EQUAL",
        content)

    content = re.sub(
        r"\bEXPECT_NE\s*\(\s*([^,]+)\s*,\s*([^)]+)\s*\)",
        r"BOOST_CHECK(\1 != \2)",
        content)



    # BOOST_CHECK_EQUAL handles strings specially automatically
    # so no need for special function like GTest's EXPECT_STREQ.
    content = re.sub(
        r"\bEXPECT_STREQ\b",
        r"BOOST_CHECK_EQUAL",
        content)

    content = re.sub(
        r"\bASSERT_STREQ\b",
        r"BOOST_REQUIRE_EQUAL",
        content)

    # FAIL() with a message streamed to it.
    content = re.sub(
        r'\bFAIL\(\)\s*<<\s*([^;]+);',
        r"BOOST_FAIL(\1);",
        content)


    outfile.write(content)


### Run the main program

if __name__ == "__main__":
    main()


# vim: set ft=python et ts=4 sw=4:
