#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Convert binary files into hex format.
#
# By Colin D Bennett, 30 May 2013.

import re
import argparse
import os
import sys


def main():
    parser = argparse.ArgumentParser(
        description="Convert binary files to hex files.")

    parser.add_argument("input", nargs="+",
                        help="specify input file")
    parser.add_argument("--output-dir", required=True,
                        help="specify output directory")
    parser.add_argument("--output-extension", default="hex",
                        help="specify output extension")
    parser.add_argument("--force", action="store_true",
                        help="overwrite existing files")
    parser.add_argument("--quiet", action="store_true",
                        help="don't warn about existing files, just skip")
    parser.add_argument("--verbose", "-v", action="store_true",
                        help="verbose output")

    args = parser.parse_args()

    for infile in args.input:
        convert_bin_to_hex_in_dir(
            infile, args.output_dir,
            out_ext=args.output_extension,
            force=args.force,
            quiet=args.quiet,
            verbose=args.verbose)


def convert_bin_to_hex_in_dir(
        infile, output_dir, out_ext, force, quiet, verbose):
    head, tail = os.path.split(infile)
    # Derive output path name from input name.
    root, ext = os.path.splitext(tail)
    # Change the extension.
    outfile = root + "." + out_ext
    # Build path.
    outpath = os.path.join(output_dir, outfile)

    if os.path.exists(outpath) and not force:
        if not quiet:
            sys.stderr.write(
                "Skipping '" + infile +
                "': Output file '" + outpath + "' already exists. " +
                "Specify --force to overwrite\n")
    else:
        convert_bin_to_hex_file(infile, outpath, verbose)


def convert_bin_to_hex_file(inpath, outpath, verbose):
    if verbose:
        sys.stderr.write("Converting " + inpath + " to " + outpath + "\n")
    with open(outpath, "w") as output:
        output.write(hexify_file(inpath))
        # Ensure there is a final newline.
        output.write("\n")


def hexify_file(infile):
    """Return ASCII hex dump string for the binary file 'infile'."""
    hexstr = ""
    bytes_on_line = 0
    with open(infile, "rb") as fin:
        b = fin.read(1)
        while b:                        # b will equal b"" (==False) at EOF
            if hexstr != "":
                if bytes_on_line == 16:
                    hexstr += "\n"      # Break line to keep fixed length
                    bytes_on_line = 0
                else:
                    if bytes_on_line == 8:
                        hexstr += " "   # Extra space between 8 byte groups
                    hexstr += " "       # Delimiter between bytes

            hexstr += "%02X" % ord(b)   # Format byte value as hex
            bytes_on_line += 1
            b = fin.read(1)
    return hexstr


# Run the main program

if __name__ == "__main__":
    main()


# vim: set ft=python et ts=4 sw=4:
