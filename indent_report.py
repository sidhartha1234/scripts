#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Produce a report on the indentation style (tabs vs. spaces) in a tree of
# files.
# Colin D Bennett, 2014-04-03.

import argparse
import os
from sys import stdout

def main():
    parser = argparse.ArgumentParser(
            description="Produce a report on indentation style.")

    parser.add_argument("paths", nargs="+", help="specify file/directory paths")
    parser.add_argument("--quiet", action="store_true", help="quiet")
    parser.add_argument("--verbose", "-v", action="store_true", help="verbose output")

    args = parser.parse_args()

    stats = WhitespaceStats()
    for path in args.paths:
        stats.process(path)


class WhitespaceStats(object):
    def __init__(self):
        stdout.write("%6s %6s %6s  %s\n" % (
            "Space", "Tab", "Mixed", "File"))

    def process(self, rootpath):
        if os.path.isdir(rootpath):
            # It's a directory, process the files in it recursively.
            for dirpath, dirnames, files in os.walk(rootpath):
                for filename in files:
                    path = os.path.join(dirpath, filename)
                    scan_ws_file(path)
        else:
            # It's a file, scan it directly
            scan_ws_file(rootpath)

def scan_ws_file(path):
    with open(path, "r") as f:
        scan_ws_stream(f, path)

# Scan an input stream for the EOL types it has.
def scan_ws_stream(f, name):
    indent_space_count = 0
    indent_tab_count = 0
    indent_mixed_count = 0

    import re
    indent_pattern = re.compile(r"^(\s+)")
    for line in f:
        match = indent_pattern.search(line)
        if match:
            indent_str = match.group(1)
            sp_num = indent_str.count(" ")
            tab_num = indent_str.count("\t")
            indent_space_count += sp_num
            indent_tab_count += tab_num
            if sp_num > 0 and tab_num > 0:
                indent_mixed_count += 1

    stdout.write("%6d %6d %6d  %s\n" % (
        indent_space_count,
        indent_tab_count,
        indent_mixed_count,
        name))


### Run the main program

if __name__ == "__main__":
    main()


# vim: set ft=python et ts=4 sw=4:
