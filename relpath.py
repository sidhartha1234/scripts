#!/usr/bin/env python
#
# relpath.py - return the path to one directory relative to another
#
# Colin D Bennett, 5 Sep 2013.
#
# References:
# - http://stackoverflow.com/a/7305217/994153

import argparse
import os
import os.path
from sys import stdout
 
def main():
    parser = argparse.ArgumentParser(
            description="Convert paths to be relative to a specific directory.")

    parser.add_argument("--relative-to", help="make path relative to this")
    parser.add_argument("paths", nargs="+", help="specify file/directory paths")

    args = parser.parse_args()

    if args.relative_to:
        # Use the specified directory from the command line option.
        relative_to = args.relative_to
    else:
        # Default to current working directory.
        relative_to = os.getcwd()

    # Process the paths.
    for path in args.paths:
        stdout.write(os.path.relpath(path, relative_to) + "\n")



### Run the main program

if __name__ == "__main__":
    main()


# vim: set ft=python et ts=4 sw=4:
