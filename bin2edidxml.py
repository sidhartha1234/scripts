#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Convert binary EDID files into EDID XML format as used by QD980 Manager.
#
# By Colin D Bennett, 28 May 2013.

import re
import argparse
import os
import sys

def main():
    parser = argparse.ArgumentParser(
            description="Convert binary EDID files to QD980 Manager XML EDID format.")

    parser.add_argument("input", nargs="+", help="specify input file")
    parser.add_argument("--output-dir", help="specify output directory")
    parser.add_argument("--force", action="store_true", help="overwrite existing files")
    parser.add_argument("--quiet", action="store_true", help="don't warn about existing files, just skip")
    parser.add_argument("--verbose", "-v", action="store_true", help="verbose output")

    args = parser.parse_args()

    for infile in args.input:
        convert_bin_to_edidxml_in_dir(infile, args.output_dir,
                force=args.force,
                quiet=args.quiet,
                verbose=args.verbose)

def convert_bin_to_edidxml_in_dir(infile, output_dir, force, quiet, verbose):
    head, tail = os.path.split(infile)
    # Derive output path name from input name.
    root, ext = os.path.splitext(tail)
    # Create the output file the appropriate extension.
    outfile = root + ".xml"
    # Build path.
    outpath = os.path.join(output_dir, outfile)

    if os.path.exists(outpath) and not force:
        if not quiet:
            sys.stderr.write("Skipping '" + infile + 
                    "': Output file '" + outpath + "' already exists. " +
                    "Specify --force to overwrite\n")
    else:
        convert_bin_to_edidxml_file(infile, outpath, verbose)

def convert_bin_to_edidxml_file(inpath, outpath, verbose):
    if verbose:
        sys.stderr.write("Converting " + inpath + " to " + outpath + "\n")

    with open(inpath, "rb") as input:
        rawdata = bytearray(input.read())  # Python 2 compat: bytearray()
        # Using bytearray() makes Python 2 'rawdata' act the same as
        # Python 3 which will be a bytes object.
        
    with open(outpath, "w") as output:
        output.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
        output.write("<DATAOBJ>\n")
        output.write("<HEADER TYPE=\"DID\" VERSION=\"1.0\"></HEADER>\n")
        output.write("<DATA>\n")

        # Constant for EDID block size.
        BLOCKSIZE = 128

        # Current index in the raw EDID.
        edidindex = 0

        for block in range(0, 4):
            # Write out BLOCKn element. Its contents might be empty
            # if the EDID doesn't have an data for this block.
            output.write("<BLOCK%d>\n" % block)

            # Number of bytes written in the block.
            blockcount = 0
            while blockcount < BLOCKSIZE and edidindex < len(rawdata):
                # Write out the hex bytes, squished together with no space
                # between bytes, as the QD980 XML syntax uses.
                output.write("%02X" % rawdata[edidindex])
                edidindex += 1
                blockcount += 1

            output.write("</BLOCK%d>\n" % block)

        output.write("</DATA>\n")
        output.write("</DATAOBJ>\n")



### Run the main program

if __name__ == "__main__":
    main()


# vim: set ft=python et ts=4 sw=4:
