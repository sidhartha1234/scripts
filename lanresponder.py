#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Listen for network discovery queries and respond.
#
# QUERY = udp packet containing "QUERY\n"
# RESPONSE = udp packet containing "ANSWER\nHost-Title: Device Name Here\nHost-Id: 123456\n"
#
# By Colin D Bennett, 31 May 2013.

import re
import argparse
import os
import sys
from sys import stdout
import socket

def main():
    parser = argparse.ArgumentParser(
            description="Listen for network discovery queries and respond.",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--listen-port", default=5488, type=int, help="port to listen on")
    parser.add_argument("--host-title", help="host title to send in response")
    parser.add_argument("--host-id", help="id to send in response")
    parser.add_argument("--verbose", "-v", action="store_true", help="verbose output")
    parser.add_argument("--quiet", "-q", action="store_true", help="be quiet, don't print output")

    args = parser.parse_args()

    # Create a UDP socket.
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Bind the UDP socket to the query listening port.
    sock.bind(('', args.listen_port))

    # Main loop
    while True:
        # Receive UDP packets up to 1024 bytes.
        data, addr = sock.recvfrom(1024)
        if args.verbose:
            stdout.write("Received message from " + str(addr) + ": " + data + "\n")

        if data.startswith("QUERY\n"):
            if not args.quiet:
                stdout.write("Responding to QUERY from %s\n" % str(addr))

            # Send an answer.
            response = "RESPONSE\n"

            if args.host_title:
                response += "Host-Title: " + args.host_title + "\n"

            if args.host_id:
                response += "Host-Id: " + args.host_id + "\n"

            sock.sendto(response, addr)


### Run the main program

if __name__ == "__main__":
    main()


# vim: set ft=python et ts=4 sw=4:
