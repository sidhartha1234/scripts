#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Align columns in CSV table, by padding with spaces.
#
# By Colin D Bennett, 21 May 2013.

import csv


def do_alignment():
    with open("input.csv", "rb") as csv_in:
        csvreader = csv.DictReader(csv_in)
        fields = csvreader.fieldnames
        records = list(csvreader)
        # Start with zero width minimum.
        #widths = [0] * len(fields)
        # Or start with field name minimum
        widths = [len(field) for field in fields]
        for recnum in range(0, len(records)):
            record = records[recnum]
            for fieldnum in range(0, len(fields)):
                fieldname = fields[fieldnum]
                fieldval = record[fieldname]
                # Ensure at least one extra space,
                # so the comma isn't touching the value.
                widths[fieldnum] = max(widths[fieldnum], len(fieldval) + 1)

        with open("output.csv", "wb") as csv_out:
            csvwriter = csv.writer(csv_out)
            csvwriter.writerow(padrecord(fields, widths))
            for dictrecord in records:
                record = []
                for field in fields:
                    record.append(dictrecord[field])
                csvwriter.writerow(padrecord(record, widths))


def padrecord(values, widths):
    return [pad(v, w) for v, w in zip(values, widths)]


def pad(s, width):
    # Right-align
    while len(s) < width:
        s = " " + s
    return s


# Run the main program
if __name__ == "__main__":
    do_alignment()
