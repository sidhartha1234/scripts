#!/bin/sh
#
# Script to execute a command for each commit in a range.
# Checks out each commit and runs the command, logging the
# results.
#
# Colin D Bennett, 11 July 2013.

# ======================================================================
# Utility functions
# ======================================================================

usage() {
    echo "Usage: $0 COMMITRANGE LOGFILE COMMAND [-n]" >&2
    echo "" >&2
    echo "Execute the given command for each Git commit in a range." >&2
    echo "" >&2
    echo "Options and arguments:" >&2
    echo "" >&2
    echo "-n  Perform a dry run, only log what would have been done." >&2
    echo "" >&2
    echo "COMMITRANGE" >&2
    echo "    A Git commit range, such as would be given" >&2
    echo "    in a command like \`git log 6866480..f1ffffa\`." >&2
    echo "" >&2
    echo "LOGFILE" >&2
    echo "    The path to a log file, to which information will be" >&2
    echo "    appended about the exec_git process." >&2
    echo "" >&2
    echo "COMMAND" >&2
    echo "    A shell command which will be passed to \`bash -c\`." >&2
    exit 1
}

die() {
    echo "Error: $1" >&2
    exit 1
}

require_clean_work_tree () {
    # Update the index
    git update-index -q --ignore-submodules --refresh
    err=0

    # Disallow unstaged changes in the working tree
    if ! git diff-files --quiet --ignore-submodules --
    then
        echo >&2 "cannot $1: you have unstaged changes."
        git diff-files --name-status -r --ignore-submodules -- >&2
        err=1
    fi

    # Disallow uncommitted changes in the index
    if ! git diff-index --cached --quiet HEAD --ignore-submodules --
    then
        echo >&2 "cannot $1: your index contains uncommitted changes."
        git diff-index --cached --name-status -r --ignore-submodules HEAD -- >&2
        err=1
    fi

    if [ $err = 1 ]
    then
        echo >&2 "Please commit or stash them."
        exit 1
    fi
}


# ======================================================================
# Main program
# ======================================================================

DRY_RUN="n"
COMMITRANGE=""
LOGFILE=""
COMMAND=""

while [ $# -ge 1 ]; do
    case "$1" in
        -n) DRY_RUN="y";;
        -*) usage;;
        *)
            if [ -z "$COMMITRANGE" ]; then COMMITRANGE="$1";
            elif [ -z "$LOGFILE" ]; then LOGFILE="$1";
            elif [ -z "$COMMAND" ]; then COMMAND="$1";
            else usage
            fi
    esac
    shift
done

[ -n "$COMMITRANGE" ] || usage
[ -n "$LOGFILE" ] || usage
[ -n "$COMMAND" ] || usage

REVLIST=$(git rev-list --reverse $COMMITRANGE --)
[ $? = 0 ] || die "rev-list failed"

require_clean_work_tree "exec_git"

echo >> "$LOGFILE"
echo >> "$LOGFILE"
echo "========================================================" >> "$LOGFILE"
echo "Starting exec-git" >> "$LOGFILE"
date >> "$LOGFILE"
echo "  COMMITRANGE: $COMMITRANGE" >> "$LOGFILE"
echo "  COMMAND: $COMMAND" >> "$LOGFILE"
echo "" >> "$LOGFILE"
echo "  COMMIT LIST:" >> "$LOGFILE"
for COMMIT in $REVLIST; do
    echo "    $COMMIT" >> "$LOGFILE"
done
echo "========================================================" >> "$LOGFILE"

# Count the number of commits we have been given.
count=0
for COMMIT in $REVLIST; do count=$(( $count + 1 )); done

# Loop and process the commits.
number=1
for COMMIT in $REVLIST; do
    echo >> "$LOGFILE"
    echo "Commit #${number} of ${count}" >> "$LOGFILE"
    git log --oneline "${COMMIT}^..${COMMIT}" >> "$LOGFILE"
    if [ "$DRY_RUN" = "n" ]; then
        if git checkout "$COMMIT"; then
            if bash -c "$COMMAND"; then
                echo " * OK" >> "$LOGFILE"
            else
                echo " * FAILED: Exec command returned failure code $?" >> "$LOGFILE"
            fi
        else
            echo " * FAILED: Checkout failed" >> "$LOGFILE"
        fi
    else
        echo " * DRY-RUN: would checkout $COMMIT" >> "$LOGFILE"
        echo " * DRY-RUN: would execute $COMMAND" >> "$LOGFILE"
    fi
    number=$(( $number + 1 ))
done


# vim: set ft=sh et sw=4 ts=4:
