#!/bin/sh
#
# hexdifftool
# Script to use KDiff3 or `git diff` on binary files, comparing the files'
# hex dump representations.
#
# To use in git, add sections like these to your .gitconfig
#
#     [difftool "hexdiffword"]
#             cmd = c:/Scripts/hexdifftool.sh --diff \"git diff --no-index --word-diff\" \"$LOCAL\" \"$REMOTE\"
#     [difftool "hexdiff"]
#             cmd = c:/Scripts/hexdifftool.sh --diff \"git diff --no-index\" \"$LOCAL\" \"$REMOTE\"
#     [difftool "exthexdiff"]
#             cmd = c:/Scripts/hexdifftool.sh --diff \"diff -u\" \"$LOCAL\" \"$REMOTE\"
#     [difftool "hexdifftool"]
#             cmd = c:/Scripts/hexdifftool.sh \"$LOCAL\" \"$REMOTE\"
# 
# and issue the command "git difftool -t hexdifftool".
#
# Colin D Bennett, 30 Sep 2013.


#
# Configure defaults
#

# There are several options for the hex dump format, and several tools to
# choose from.  The srec_cat output looks like
# 00000000: 00 FF FF FF FF FF FF 00 05 B8 00 06 A0 86 01 00  #.........8.. ...
# 00000010: 28 17 01 03 80 00 00 78 0E EE 95 A3 54 4C 99 26  #(......x.n.#TL.&
# Other good options include "od -t x1 -A x" and "hexdump -C".

# Usage: hexdump INPUT.bin OUTPUT.txt
hexdump() {
    srec_cat "$1" -binary -o "$2" -hex-dump
}


DIFF_COMMAND="kdiff3"

#
# Define utility functions
#

die() {
    echo "Error: $1" >&2
    exit 1
}

usage() {
    echo "Usage: $0 [--diff DIFFTOOL] FILEA FILEB [FILEC]" >&2
    echo "" >&2
    echo "Compares the hex-dump of binary file FILEA to FILEB." >&2
    exit 1
}

#
# Parse command line arguments
#

if [ "$1" = "--diff" ]; then
    shift  # Eat the --diff
    DIFF_COMMAND="$1"
    shift  # Eat the DIFF_COMMAND value
fi

#
# Main program
#

if [ $# -eq 2 ]; then
    # standalone diff2 mode
    FILE1="$1"
    FILE2="$2"
elif [ $# -eq 3 ]; then
    # standalone diff3 mode
    FILE1="$1"
    FILE2="$2"
    FILE3="$3"
    # need to identify which is the BASE and which is OURS/THEIRS.
else
    usage
fi


# OK, we got FILE1 and FILE2 set up.
tmpfile1="_hex$$_1"
tmpfile2="_hex$$_2"
tmpfile3="_hex$$_3"
error=0

hexdump "$FILE1" "$tmpfile1" || error=1
hexdump "$FILE2" "$tmpfile2" || error=1
if [ -n "$FILE3" ]; then
    hexdump "$FILE3" "$tmpfile3" || error=1
fi


if [ "$error" = 0 ]; then
    # OK, show the comparison.

    # Status code for standard diff is:
    # 0 - files are identical
    # 1 - files differ
    # 2 - trouble
    status=0
    if [ "$DIFF_COMMAND" = "kdiff3" ]; then
        # For kdiff3, use --L1, --L2, --L3 to specify names
        if [ -z "$FILE3" ]; then
            $DIFF_COMMAND -L "$FILE1" "$tmpfile1" \
                          -L "$FILE2" "$tmpfile2"
            status="$?"
        else
            $DIFF_COMMAND -L "$FILE1" "$tmpfile1" \
                          -L "$FILE2" "$tmpfile2" \
                          -L "$FILE3" "$tmpfile3"
            status="$?"
        fi
    elif [ "$DIFF_COMMAND" = "diff" ]; then
        # Standard POSIX diff command.
        if [ -z "$FILE3" ]; then
            $DIFF_COMMAND -L "$FILE1" "$tmpfile1" \
                          -L "$FILE2" "$tmpfile2"
            status="$?"
        else
            # Standard POSIX diff3 command?
            # need to specify? Not yet implemented.
            status="2"   # indicate "trouble"
        fi
    else
        # Other diff tool, call it with just the text file names.
        if [ -z "$FILE3" ]; then
            $DIFF_COMMAND "$tmpfile1" \
                          "$tmpfile2"
            status="$?"
        else
            $DIFF_COMMAND "$tmpfile1" \
                          "$tmpfile2" \
                          "$tmpfile3"
            status="2"   # indicate "trouble"
        fi
    fi

    rm -f "$tmpfile1" "$tmpfile2" "$tmpfile3"

    if [ "$status" -ne 0 ] && [ "$status" -ne 1 ]; then
        die "diff command failed (return code ${status})"
    fi
else
    # Error
    rm -f "$tmpfile1" "$tmpfile2" "$tmpfile3"
    die "hexdump failed"
fi



# vim: set et ts=4 sw=4 textwidth=74:
