#!/bin/sh
#
# Show EOL report for each version of a file committed to git.
#
# Colin D Bennett, 4 Sep 2013.
#


die() {
    echo "Error: $1" >&2
    exit 1
}

usage() {
    echo "Usage: $0 commit file" >&2
    echo "  commit   - the starting commit" >&2
    echo "  file     - the file to analyze" >&2
    exit 1
}

[ $# -ge 1 ] || die "need commit arg"
STARTING_COMMIT="$1"
shift

[ $# -ge 1 ] || die "need file arg"
FILE="$1"
shift


TMPDIR="tmp.$$"
mkdir "$TMPDIR" || die "couldn't mkdir $TMPDIR"
count=1
for commit in $(git log --pretty="%H" "$STARTING_COMMIT" -- "$FILE"); do
    shorthash=$(echo "$commit" | cut -c 1-7)
    # shortmsg needs to be sanitized for file name, remove quotes and
    # other characters that might be trouble
    #shortmsg=$(git log -1 --pretty="%s" "$commit" | cut -c 1-40)
    countstr=$(printf "%03d" "$count")
    outfile="${TMPDIR}/${countstr}-${shorthash}"
    git show "${commit}:${FILE}" > "$outfile"
    count=$(( $count + 1 ))
done

# Run eol_report.py from TMPDIR so that TMPDIR doesn't appear in
# the file name output column.
(cd "$TMPDIR" && python /c/Scripts/eol_report.py *)

rm -r "$TMPDIR"


# vim: set et ts=4 sw=4:
