#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Scan for lanresponder devices.
#
# QUERY = udp packet containing "QUERY\n"
# RESPONSE = udp packet containing "ANSWER\nHost-Title: Device Name Here\nHost-Id: 123456\n"
#
# By Colin D Bennett, 31 May 2013.

import re
import argparse
import os
import sys
from sys import stdout
from sys import stderr
import socket

def main():
    parser = argparse.ArgumentParser(
            description="Scan network for lanresponder services.",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--query-port", default=5488, type=int, help="UDP port to which queries are sent")
    parser.add_argument("--timeout", default=5, type=int, help="Time in seconds to wait for a response")
    parser.add_argument("--verbose", "-v", action="store_true", help="verbose output")

    args = parser.parse_args()

    # Create a UDP socket.
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Bind the UDP socket to a random port to receive responses.
    sock.bind(('', 0))

    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1) 

    # Broadcast the query.
    if args.verbose:
        stderr.write("INFO: Sending query\n")
    message = "QUERY\n"
    sock.sendto(message, ("<broadcast>", args.query_port))
    
    # Set a timeout for receive operations.
    sock.settimeout(args.timeout)

    # Main loop
    num_hosts_found = 0
    try:
        # Keep track of whether column headings have been printed.
        header_printed = False

        while True:
            # Receive UDP packets up to 1024 bytes.
            data, addr = sock.recvfrom(1024)
            if args.verbose:
                stderr.write("INFO: Received message from " + str(addr) + ": " + data + "\n")
            
            if data.startswith("RESPONSE\n"):
                if args.verbose:
                    stderr.write("INFO: Got a response from " + str(addr) + "\n")

                # Get the IP host and the UDP port number.
                host = addr[0]
                port = addr[1]

                titlematch = re.search("^Host-Title: (.*)$", data, re.MULTILINE)
                if titlematch is None:
                    title = "-"
                else:
                    title = titlematch.group(1)

                idmatch = re.search("^Host-Id: (.*)$", data, re.MULTILINE)
                if idmatch is None:
                    id = "-"
                else:
                    id = idmatch.group(1)

                if not header_printed:
                    # Print the column headings before the first row.
                    stdout.write("%-16s %-24s %-24s\n" % (
                            "IP Address", "Title", "Host ID"))
                    header_printed = True

                stdout.write("%-16s %-24s %-24s\n" % (
                        host, title, id))

                num_hosts_found += 1

    except socket.timeout:
        # OK, receive timeout elapsed.
        pass

    # Print the number of hosts found.
    plural_hosts = 's'
    if num_hosts_found == 1:
        plural_hosts = ''
    sys.stdout.write("\nDone. Found %d host%s.\n" % (
            num_hosts_found, plural_hosts))


### Run the main program

if __name__ == "__main__":
    main()


# vim: set ft=python et ts=4 sw=4:
