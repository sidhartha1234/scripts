# CDB Scripts

This is a collection of Python scripts (and a few shell scripts) that
provide some specific utility.

Examples:

- `bin2hex.py *.bin`: convert binary files (`*.bin`) to hex ASCII format
  and save as `*.hex` in the current directory.

- `eol_report mydir`: display line-ending information for files under
  the directory tree `mydir`.

- `filter-path --exclude local/bin -- someprogram`: Run the program
  `someprogram` with modified `PATH` environment variable, where
  `/usr/local/bin` (and any other element containing the regex `local/bin`) is
  removed.

Most of these scripts will run on both Python 2 and Python 3, and on
both Windows and Linux.
